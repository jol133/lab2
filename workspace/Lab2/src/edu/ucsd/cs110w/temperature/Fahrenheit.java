/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author kegu
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete
		return (String.valueOf(getValue())+ " F");
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((getValue()-32)/(float)1.8);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin((getValue()-32)/(float)1.8+(float)273.15);
	}
}
