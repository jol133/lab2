package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO Complete
		return (String.valueOf(getValue()) + " C");
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(getValue()*(float)1.8+32);
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return new Kelvin(getValue()+(float)273.15);
	}

}
