package edu.ucsd.cs110w.tests;
import edu.ucsd.cs110w.temperature.Temperature;
import edu.ucsd.cs110w.temperature.Kelvin;
import junit.framework.TestCase;

public class KelvinTests extends TestCase {
	private float delta = 0.001f;
	public void testKelvin(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testKelvinToString(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " K";
		// Verify the suffix of the formatted string
		assertTrue(string.startsWith(beginning));
		// Verify the prefix of the formatted string
		assertTrue(string.endsWith(ending));
		// Verify the middle of the formatted string
		int endIndex = string.indexOf(ending);
		// (Hint: what is the length of the middle of the string?)
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}
	public void testKelvinToKelvin()
	{
		Kelvin temp = new Kelvin(32);
		Temperature convert = temp.toKelvin();
		assertEquals(32,convert.getValue(), delta);
		
	}
	public void testKelvinToCelsius(){
		Kelvin temp = new Kelvin(32);
		Temperature convert = temp.toCelsius();
		assertEquals(-241.15, convert.getValue(), delta);
		temp = new Kelvin(212);
		convert = temp.toCelsius();
		assertEquals(-61.15, convert.getValue(), delta);
	}
	public void testKelvintoFahrenheit()
	{
		Kelvin temp = new Kelvin(32);
		Temperature convert = temp.toFahrenheit();
		assertEquals(-402.07, convert.getValue(), delta);
		temp = new Kelvin(212);
		convert = temp.toFahrenheit();
		assertEquals(-78.07, convert.getValue(), delta);
	}
}
